// MIT License

// Copyright (c) 2018 finixbit
// Copyright (c) 2022 Colton Pawielski

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "elf_parser.hpp"

#include <algorithm>
#include <cassert>
#include <fstream>
#include <iterator>

using namespace elfparser;

std::vector<ElfSection> ElfParser::get_sections()
{
    const Elf32_Ehdr* ehdr = get_elf_header();
    const Elf32_Shdr* shdr = (Elf32_Shdr*)(get_base_address() + ehdr->e_shoff);
    uint32_t shnum = ehdr->e_shnum;

    const Elf32_Shdr* sh_strtab = &shdr[ehdr->e_shstrndx];
    const char* const sh_strtab_p = get_base_address() + sh_strtab->sh_offset;

    std::vector<ElfSection> sections;
    for (uint32_t i = 0; i < shnum; ++i) {
        ElfSection section(m_elfData, i, std::string(sh_strtab_p + shdr[i].sh_name), shdr[i]);
        sections.push_back(section);
    }
    return sections;
}

std::vector<ElfSegment> ElfParser::get_segments()
{
    const Elf32_Ehdr* ehdr = get_elf_header();
    const Elf32_Phdr* phdr = (const Elf32_Phdr*)(get_base_address() + ehdr->e_phoff);
    int phnum = ehdr->e_phnum;

    std::vector<ElfSegment> segments;
    for (int i = 0; i < phnum; ++i) {
        ElfSegment segment(phdr[i]);
        segments.push_back(segment);
    }
    return segments;
}

std::vector<char> ElfParser::get_bin()
{
    auto sections = get_sections();

    // remove non-loadable sections
    auto end_valid = std::remove_if(sections.begin(), sections.end(), 
        [](const elfparser::ElfSection& section) 
        {
            // elf section must be alloc
            bool isAlloc = (section.get_flags() & SHF_ALLOC);
            // only care about elf sections that have data
            bool hasData = (section.get_size() > 0);
            return !(isAlloc && hasData);
        }
    );
    sections.erase(end_valid, sections.end());

    // remove and warn about invalid ram sections
    end_valid = std::remove_if(sections.begin(), sections.end(), 
        [](const elfparser::ElfSection& section) 
        {
            // section must be above starting address
            bool isValidRam = (section.get_addr() >= 0x8C010000);
            if (!isValidRam) 
                std::cout << "WARN: dropping section " << section.get_name() << " at 0x" << std::hex 
                          << section.get_addr() << std::dec << " due to being outside of valid ram" << std::endl;
            return !isValidRam;
        }
    );
    sections.erase(end_valid, sections.end());

    // get begining and end section to determine bin size
    auto min_max = std::minmax_element(sections.begin(), sections.end(), 
        [](const elfparser::ElfSection& left, const elfparser::ElfSection& right){
            return left.get_addr() < right.get_addr();
        }
    );

    size_t start = min_max.first->get_addr();
    size_t end = min_max.second->get_addr() + min_max.second->get_size();

    // build bin
    std::vector<char> bin_data(end - start);
    for (elfparser::ElfSection section : sections) {
        auto offset = section.get_addr() - start;
        memcpy(&bin_data[offset], &section.data_ptr()[0], section.get_size());
    }
    return bin_data;
}

std::optional<std::shared_ptr<ElfParser>> ElfParser::Load(const std::vector<char>& elfData)
{
    const Elf32_Ehdr* header = (const Elf32_Ehdr*)&elfData[0];

    if( header->e_machine != EM_SH ) {
        std::cout << "Unsupported Architecture" << std::endl;
        return {};
    }

    if(header->e_ident[EI_CLASS] != ELFCLASS32) {
        std::cout << "Unsupported ELF" << std::endl; 
        return {};
    }

    return std::shared_ptr<ElfParser>(new ElfParser(elfData));
}

std::optional<std::shared_ptr<ElfParser>> ElfParser::Load(const std::filesystem::path& elfPath)
{
    auto size = static_cast<int32_t>(std::filesystem::file_size(elfPath));
    std::ifstream stream;
    stream.open(elfPath, std::ios::binary);
    if (!stream.is_open()) {
        std::cerr << "Failed to open elf at " << elfPath << std::endl;
        return {};
    }
    std::vector<char> elf_data(size);
    stream.read(&elf_data[0], size);
    return Load(elf_data);
}