// MIT License

// Copyright (c) 2018 finixbit
// Copyright (c) 2022 Colton Pawielski

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef H_ELF_PARSER
#define H_ELF_PARSER

#include <cstdio>
#include <cstdlib>
#include <fcntl.h> /* O_RDONLY */
#include <fcntl.h>
#include <filesystem>
#include <iostream>
#include <memory>
#include <optional>
#include <string.h>
#include <string>
#include <vector>
#include "elf.h"

namespace elfparser {

class ElfSection {
protected:
    uint32_t m_Type, m_Flags, m_Index, m_Size, m_EntSize, 
             m_AddrAlign, m_Offset, m_Address;
    std::string m_Name;
    std::vector<char> m_Data;

public:
    ElfSection(const std::vector<char> elf, uint32_t index, std::string name, const Elf32_Shdr& shdr)
        : m_Type(shdr.sh_type)
        , m_Flags(shdr.sh_flags)
        , m_Index(index)
        , m_Size(shdr.sh_size)
        , m_EntSize(shdr.sh_entsize)
        , m_AddrAlign(shdr.sh_addralign)
        , m_Offset(shdr.sh_offset)
        , m_Address(shdr.sh_addr)
        , m_Name(name)
    {
        m_Data.resize(shdr.sh_size);
        memcpy(&m_Data[0], &elf[0] + shdr.sh_offset, shdr.sh_size);
    }

    int get_index() const { return m_Index; }
    std::string get_name() const { return m_Name; }
    uint32_t get_flags() const { return m_Flags; }
    uint32_t get_addr() const { return m_Address; }
    uint32_t get_offset() const { return m_Offset; }
    uint32_t get_size() const { return m_Size; }
    uint32_t get_entsize() const { return m_EntSize; }
    uint32_t get_alignment() const { return m_AddrAlign; }
    std::vector<char> data_ptr() const { return m_Data; };
    std::string get_type() const {           
        switch (m_Type) {
            case 0: return "SHT_NULL"; /* Section header table entry unused */
            case 1: return "SHT_PROGBITS"; /* Program data */
            case 2: return "SHT_SYMTAB"; /* Symbol table */
            case 3: return "SHT_STRTAB"; /* String table */
            case 4: return "SHT_RELA"; /* Relocation entries with addends */
            case 5: return "SHT_HASH"; /* Symbol hash table */
            case 6: return "SHT_DYNAMIC"; /* Dynamic linking information */
            case 7: return "SHT_NOTE"; /* Notes */
            case 8: return "SHT_NOBITS"; /* Program space with no data (bss) */
            case 9: return "SHT_REL"; /* Relocation entries, no addends */
            case 11: return "SHT_DYNSYM"; /* Dynamic linker symbol table */
            default: return "UNKNOWN";
        }
     }
};

class ElfSegment {
protected:
    uint32_t m_Type, m_Offset, m_VirtAddr, m_PhysAddr, 
        m_FileSize, m_Flags, m_Align;

public:
    ElfSegment(const Elf32_Phdr &phdr) 
        : m_Type(phdr.p_type)
        , m_Offset(phdr.p_offset)
        , m_VirtAddr(phdr.p_vaddr)
        , m_PhysAddr(phdr.p_paddr)
        , m_FileSize(phdr.p_filesz)
        , m_Flags(phdr.p_flags)
        , m_Align(phdr.p_align)
        {

    }

    uint32_t get_offset() const { return m_Offset; }
    uint32_t get_virtaddr() const { return m_VirtAddr; }
    uint32_t get_physaddr() const { return m_PhysAddr; }
    uint32_t get_filesize() const { return m_FileSize; }
    uint32_t get_align() const { return m_Align; }

    std::string get_segment_type() const
    {
        switch (m_Type) {
            case PT_NULL: return "NULL"; /* Program header table entry unused */
            case PT_LOAD: return "LOAD"; /* Loadable program segment */

            default: return "UNKNOWN";
        }
    }

    std::string get_segment_flags() const
    {
        std::string flags;

        if (m_Flags & PF_R)
            flags.append("R");

        if (m_Flags & PF_W)
            flags.append("W");

        if (m_Flags & PF_X)
            flags.append("E");

        return flags;
    }
};

class ElfParser {
private:
    const std::vector<char> m_elfData;

public:
    // Load ELF from std::vector<char>
    static std::optional<std::shared_ptr<ElfParser>> Load(const std::vector<char>& elfData);

    // Load ELF from Path
    static std::optional<std::shared_ptr<ElfParser>> Load(const std::filesystem::path& elfData);

    std::vector<ElfSection> get_sections();
    std::vector<ElfSegment> get_segments();

    std::vector<char> get_bin();
    std::vector<char> get_elf() { return m_elfData; };
    std::size_t get_elf_size() { return m_elfData.size(); };

protected:
    ElfParser(std::vector<char> elfData)
        : m_elfData(elfData) {};

    const char* get_base_address() { return &m_elfData[0]; }
    const Elf32_Ehdr* get_elf_header() { return (const Elf32_Ehdr*)get_base_address(); }
};

} // namespace elfparser
#endif